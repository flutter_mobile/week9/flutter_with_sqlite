import 'package:flutter/material.dart';
import 'package:persist_data_with_sqlite/cat.dart';
import 'dart:async';
import 'cat_dao.dart';
import 'dog.dart';
import 'dog_dao.dart';

void main() async {
  var fido = Dog(id: 0, name: "Fido", age: 35);
  var dido = Dog(id: 1, name: "Dido", age: 20);
  await DogDao.insertDog(fido);
  await DogDao.insertDog(dido);

  print(await DogDao.dogs());

  fido = Dog(id: fido.id, name: fido.name, age: fido.age + 7);
  await DogDao.updateDog(fido);
  print(await DogDao.dogs());

  await DogDao.deletDog(0);
  print(await DogDao.dogs());

  var nido = Cat(id: 0, name: "Nido", age: 35);
  var vido = Cat(id: 1, name: "Vido", age: 20);
  await CatDao.insertCat(nido);
  await CatDao.insertCat(vido);

  print(await CatDao.cats());

  nido = Cat(id: nido.id, name: nido.name, age: nido.age + 7);
  await CatDao.updateCat(nido);
  print(await CatDao.cats());

  await CatDao.deletCat(0);
  print(await CatDao.cats());
}
