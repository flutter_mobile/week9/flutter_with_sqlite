import 'package:sqflite/sqflite.dart';

import 'database_provider.dart';
import 'dog.dart';

class DogDao {
  static Future<void> insertDog(Dog dog) async {
    final db = await DatabaseProvider.databaseDog;
    await db.insert("dogs", dog.toMap(),
        conflictAlgorithm: ConflictAlgorithm.replace);
  }

  static Future<List<Dog>> dogs() async {
    final db = await DatabaseProvider.databaseDog;
    return Dog.toList(await db.query(
      "dogs",
    ));
  }

  static Future<void> updateDog(Dog dog) async {
    final db = await DatabaseProvider.databaseDog;

    await db.update(
      "dogs",
      dog.toMap(),
      where: "id=?",
      whereArgs: [dog.id],
    );
  }

  static Future<void> deletDog(int id) async {
    final db = await DatabaseProvider.databaseDog;
    await db.delete(
      "dogs",
      where: "id=?",
      whereArgs: [id],
    );
  }
}
