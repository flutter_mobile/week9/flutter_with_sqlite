import 'package:persist_data_with_sqlite/cat.dart';
import 'package:sqflite/sqflite.dart';

import 'database_provider.dart';
import 'cat.dart';

class CatDao {
  static Future<void> insertCat(Cat cat) async {
    final db = await DatabaseProvider.databaseCat;
    await db.insert("cats", cat.toMap(),
        conflictAlgorithm: ConflictAlgorithm.replace);
  }

  static Future<List<Cat>> cats() async {
    final db = await DatabaseProvider.databaseCat;
    return Cat.toList(await db.query(
      "cats",
    ));
  }

  static Future<void> updateCat(Cat cat) async {
    final db = await DatabaseProvider.databaseCat;

    await db.update(
      "cats",
      cat.toMap(),
      where: "id=?",
      whereArgs: [cat.id],
    );
  }

  static Future<void> deletCat(int id) async {
    final db = await DatabaseProvider.databaseCat;
    await db.delete(
      "cats",
      where: "id=?",
      whereArgs: [id],
    );
  }
}
