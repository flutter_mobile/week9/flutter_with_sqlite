import 'package:flutter/material.dart';
import 'dart:async';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class DatabaseProvider {
  static Future<Database>? _databaseDog;
  static Future<Database>? _databaseCat;
  static Future<Database> get databaseDog {
    return _databaseDog ?? initDogDB();
  }

  static Future<Database> get databaseCat {
    return _databaseCat ?? initCatDB();
  }

  static Future<Database> initDogDB() async {
    WidgetsFlutterBinding.ensureInitialized();
    _databaseDog = openDatabase(
      join(await getDatabasesPath(), 'doggie_database.db'),
      onCreate: (db, version) {
        return db.execute(
            "CREATE TABLE dogs(id INTEGER PRIMARY KEY, name TEXT,age INTEGER)");
      },
      version: 1,
    );
    return _databaseDog as Future<Database>;
  }

  static Future<Database> initCatDB() async {
    WidgetsFlutterBinding.ensureInitialized();
    _databaseCat = openDatabase(
      join(await getDatabasesPath(), 'cats_database.db'),
      onCreate: (db, version) {
        return db.execute(
            "CREATE TABLE cats(id INTEGER PRIMARY KEY, name TEXT,age INTEGER)");
      },
      version: 1,
    );
    return _databaseCat as Future<Database>;
  }
}
